#!/bin/bash

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

# Copyright 2022, Tarek Shehab

###########################################################################################

# Dies ist die Projektarbeit von Tarek Shehab zum Kurs LPIC1 von Alfatraining im März 2022
# Kompliziertere Code-Blöcke habe ich kommentiert, selbsterklärende Einzeiler
# bleiben größtenteils unkommentiert.

###########################################################################################

option1="Partitionierung anzeigen"
option2="Prozessliste anzeigen"
option3="Dateien anzeigen, bei denen das SUID-Bit gesetzt ist"
option4="Drei neue User anlegen"
option5="IP-Adressen aller momentan konfigurierten Schnittstellen anzeigen"
option6="Kernelversion anzeigen"
option7="Uhrzeit und Zeitzone anzeigen"
option8="Freien und verwendeten Speicherplatz auf / anzeigen"
option9="Belegten Festplattenplatz eines jeden  ~/ dirs aller User anzeigen"
option10="Beenden"

PS3=$'\n'"Bitte einen Menüpunkt wählen. > "

function debug_exit() {
	echo "exiting for debugging purposes."
	exit 0;
}

function add_three_users() {
	# reinitialisiere die variablen, um eingaben aus vorherigen durchgängen zu löschen
	user_input_file=""
	usernames_array=()

	echo -e "\nOK, $option4."
	echo -e "\nBitte geben Sie den absoluten Pfad zu einer Datei ein,"
	echo -e "welche - zeilensepariert - die drei Benutzernamen enthält."

	# zeige einen prompt, solange $user_input_file eine leere variable ist
	while [[ -z "$user_input_file" ]]; do
		echo -n "---> "
		read user_input_file
	done

	# wir müssen die datei einiger prüfungen unterziehen,
	# bevor wir ihren inhalt auf "useradd" loslassen können.
	# prüfe ob datei existiert
	if test -f $user_input_file; then
		# prüfe datei auf exakt drei zeilen
		if [[ $(wc -l ${user_input_file} | cut -d" " -f1) != 3 ]]; then
			echo -e "\nDie Datei ${user_input_file} besteht nicht aus exakt drei Zeilen."
			any_key_to_continue
		# prüfe datei auf whitespace
		elif [[ $(grep -c [[:space:]] ${user_input_file}) > 0 ]]; then
			echo -e "\nDie Datei ${user_input_file} darf keine Leerzeichen oder Tabs enthalten."
			any_key_to_continue
		fi

		# mehr prüfungen
		# while read mit input redirection, anstatt wie ursprünglich:
		# for i in $(cat file)
		# das führt zu ungewollten resultaten, wenn shell wildcards
		# (z.B. *) als username angegeben werden.
		# siehe:
		# https://mywiki.wooledge.org/DontReadLinesWithFor
		while read username; do
			# prüfe ob user bereits existiert
			#
			# wichtig sind hier die beiden zeichen "^" und "$" vor
			# und nach dem aufruf der variable $username. Ohne
			# diese zeichen würde das script fälschlicherweise
			# einen fehler werfen, wenn ein benutzer namens (z.B.)
			# "micha" angelegt werden soll, auf dem system aber
			# bereits ein benutzer namens "michael" exisitert, weil
			# der string "micha" in "michael" enthalten ist. durch
			# die start- und end-anker (^ + $) suchen wir nach
			# einem exakten treffer für den uns relevanten
			# benutzernamen aus $user_input_file.
			if [[ $(cut -d: -f1 /etc/passwd | grep -c ^"${username}"$) == 1 ]]; then
				echo -e "\nDer Benutzer "${username}" existiert bereits auf diesem System."
			# prüfe die namen auf POSIX validität mit ein bisschen regex magic
			elif [[ $(echo "${username}" | \
		            grep -E "^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$") != "${username}"  ]]; then
				echo -e "\nDer Benutzername "${username}" ist nicht POSIX-valide."
			else
				# wir legen ein array an, was später nur
				# ausgewertet wird, wenn es ganau drei elemente
				# lang ist. So vermeiden wir, dass z.B. zwei
				# user angelegt werden, bevor wir feststellen,
				# dass der dritte ungültig ist. das script soll
				# ja genau drei user anlegen.
				usernames_array+=("${username}")
			fi
		done < ${user_input_file}
		echo
		if [[ ${#usernames_array[@]} == 3 ]]; then
			for name in ${usernames_array[@]}
			do
				echo "Lege Benutzer ${name} an."
				useradd ${name} && \
					echo "${name}:123" | chpasswd && \
					passwd -e ${name} > /dev/null
			done
			echo -e "\nAlle Benutzer erstellt.\nPasswort ist 123."
			any_key_to_continue
		else
			echo "Ein oder mehrere Namen sind noch fehlerhaft. Bitte berichtigen und Vorgang wiederholen."
			any_key_to_continue
		fi
	else
		echo -e "\n${user_input_file} nicht gefunden. Keine solche Datei."
		any_key_to_continue
fi
}

function show_ip_addresses() {
	# das ist ein bisschen hacky, gebe ich zu. :-)
	# aber einen einfacheren weg habe ich nicht gefunden
	# wir lesen den output des gefilterten "ip" befehls in einen while loop
	# ein, und definieren dann per "read" befehl die zwei für uns
	# relevanten variablen (interface und ip_addr. die variable state
	# brauchen wir für die aufgabe nicht, sie muss aber mit definiert
	# werden). Mit dem "echo" befehl geben wir dann, gemäß der
	# aufgabenstellung, die schnittstellen mit dazugehörigen ips aus.
	while read line
	do
		read interface state ip_addr < <(echo $line)
		echo "Die Schnittstelle $interface hat momentan die IP $ip_addr."
	done < <(ip -br -o -4 a | tr -s " " | cut -d/ -f-1)
}

function any_key_to_continue() {
	echo
	read -n 1 -s -r -p "Eine Taste drücken, um zum Menü zurückzukehren..."
	show_menu
}

function show_menu() {
	clear
	menu_options=(
	"$option1"
	"$option2"
	"$option3"
	"$option4"
	"$option5"
	"$option6"
	"$option7"
	"$option8"
	"$option9"
	"$option10")

	echo -e "\n\n***************"
	echo -e "   Hauptmenü"
	echo -e "***************\n"


	select option in "${menu_options[@]}"
	do
	case $option in
		$option1)
			echo -e "\nOK. $option1.\n"
			fdisk -l
			any_key_to_continue
			;;
		$option2)
			echo -e "\nOK. $option2.\n"
			ps aux | less
			any_key_to_continue
			;;
		$option3)
			echo -e "\nOK. $option3.\n"
			find / -perm -u+s 2> /dev/null
			any_key_to_continue
			;;
		$option4)
			add_three_users
			;;
		$option5)
			echo -e "\nOK. $option5.\n"
			show_ip_addresses
			any_key_to_continue
			;;
		$option6)
			echo -e "\nOK. $option6.\n"
			echo Die momentane Kernelversion ist $(uname -r).
			any_key_to_continue
			;;
		$option7)
			echo -e "\nOK. $option7.\n"
			echo "Es ist jetzt $(date +%X), die verwendete Zeitzone ist $(readlink /etc/localtime | cut -d"/" -f5,6)."
			any_key_to_continue
			;;
		$option8)
			echo -e "\nOK. $option8.\n"
			# wir filtern uns den output von "df -h /" so zurecht,
			# dass er nur die für uns relevante zeile enthält, und
			# schieben ihn dann per input redirection in den "read"
			# befehl, sodass wir alle benötigten variablen in einer
			# zeile einlesen können.
			read filesystem size used avail use_pct mount_point < <(df -h / | tail -n1)
			echo -e "Auf / sind $used von $size belegt."
			echo -e "Dies entspricht einer Belegung von $use_pct Prozent, es sind noch $avail verfügbar."
			any_key_to_continue
			;;
		$option9)
			echo -e "\nOK. $option9.\n"
			# wir gehen mit dem "du" befehl ein level tief in das
			# verzeichnis "/home/", danach filtern wir den eintrag
			# für das verzeichnis selber aus unserem output heraus,
			# da in der aufgabenstellung nur nach den
			# unterverzeichnissen gefragt wird.
			du -h -d1 /home/ | grep -v "home/$"
			any_key_to_continue
			;;
		$option10)
			echo -e "\nAlles klar! Programm wird beendet.\nVielen Dank!"
			exit 0;
	esac
	done
}

show_menu
