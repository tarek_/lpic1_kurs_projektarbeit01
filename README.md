Projektaufgabe für den Kurs LPIC-1

Aufgabenstellung:

Schreiben Sie ein Shellscript mit Menüführung. Es sollen 10 Menüpunkte zur Verfügung stehen, wobei der letzte Menüpunkt die M[glichkeit bieten soll, das Script sauber zu verlassen. Das Script soll sich nach der Ausführung eines Menüpunktes nicht beenden, sondern wieder zur Menüauswahl zurückkehren.

Nach der Ausführung eines Menüpunktes soll die Meldung „Eine Taste drücken, um zum Menü zurückzukehren...“ ausgegeben werden, nach dem Druck einer beliebigen Taste soll dann wieder das Menü erscheinen.

Folgende Aufgaben sollen über das Shellscript ausgeführt werden k[nnen, jeweils nach Auswahl des entsprechenden Menüpunktes:

1.) Die momentane Festplattenaufteilung (Partitionierung) anzeigen lassen

2.) Die Prozessliste anzeigen lassen. Hierbei sollen alle Prozesse angezeigt werden, auch die anderer Benutzer und ebenfalls die Prozesse, die nicht mit einer Konsole verbunden sind (Daemons)

3.) Auffinden und anzeigen lassen aller auf dem System vorhandenen Dateien, die das SUID-Bit gesetzt haben. Folgenden Verzeichnisse sollen dabei nicht durchsucht werden: /proc, /sys, /dev.

4.) Anlegen von Benutzern aus einer Liste (Textdatei) heraus. In der Liste sollen sich drei neue Nutzernamen befinden. Das Script soll den vollst]ndigen Pfad zu dieser Liste abfragen und dann die Benutzer anlegen. Die Benutzer sollen ein Standard-Passwort bekommen, außerdem soll dieses Standard-Passwort sofort ablaufen, um die Nutzer zu zwingen, direkt nach dem ersten Login das Passwort zu ]ndern.

5.) Die IP-Adressen aller momentan konfigurierten Schnittstellen anzeigen lassen. Die Ausgabe soll in dem folgenden Format erfolgen:<br/><br/>
**Die Schnittstelle eth0 hat momentan die IP 192.168.178.22<br/>
Die Schnittstelle eth1 hat momentan die IP 192.168.178.23...usw.**<br/>

6.) Die momentane Kernelversion anzeigen lassen.

7.) Die Uhrzeit sowie die aktuelle Zeitzone im folgenden Format ausgeben lassen:<br/><br/>
**Es ist jetzt 09:46:35, die verwendete Zeitzone ist Europe/Berlin**

8.) Den freien und verwendeten Festplattenspeicher des Wurzelverzeichnisses anzeigen lassen. Die Ausgabe soll in folgendem Format erfolgen:<br/><br/>
**Auf / sind 7,3G von 38G belegt.<br/>
Dies entspricht einer Belegung von 21% Prozent, es sind noch 29G verfügbar.**<br/><br/>
Diese Ausgabe MUSS auf JEDEM System funktionieren! Bedenken Sie, dass das
Wurzelverzeichnis auf einer beliebigen Partition liegen kann (z. B. sda1, vdb6, /dev/mapper/debian—clt—vg-root, /dev/md0 usw.).

9.) Den Platzbedarf jedes einzelnen Benutzerverzeichnisses unter /home ermitteln und anzeigen lassen. Die Ausgabe soll in etwa so aussehen:<br/><br/>
**60K /home/brit<br/>
48K /home/fred<br/>
24K /home/hallo<br/>
101M /home/user**<br/>

10.) Das Script beenden.

Jeder einzelne Menüpunkt des Scriptes muss im Sinne der Aufgabenstellung funktionieren! Da dieses Script Operationen und Befehle beinhaltet, die nur für den root-Benutzer verfügbar sind, wird davon ausgegangen, dass dieses Script als root oder per sudo ausgeführt wird. Getestet wird das Script dann auf einer aktuellen Debian-Version.<br/><br/>
Viel Erfolg!
